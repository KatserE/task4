OUTPUT	:= "./output/"
SRC		:= "task4.swift"
NAME	:= "task4"

all:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)
	@swiftc -O $(SRC) -o $(OUTPUT)/$(NAME)
	@echo Compilation is successful!

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
