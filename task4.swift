// MARK: - Create 2 enums with a different RawValue type;
// MARK: Months enum. Raw type is Int
enum MonthsInt : Int, CaseIterable {
    case January, February
    case March, April, May
    case June, July, August
    case September, October, November
    case December
}

// MARK: Print default values of an int enum
print("Default values of MonthsInt enum:")
MonthsInt.allCases.forEach({ print("\($0) -- \($0.rawValue)") })

// MARK: Months enum. Raw type is String
enum MonthsString : String, CaseIterable {
    case January, February
    case March, April, May
    case June, July, August
    case September, October, November
    case December
}

// MARK: Print default values of an string enum
print("\nDefault values of MonthsString enum:")
MonthsString.allCases.forEach { print("\($0) -- \($0.rawValue)") }


// MARK: - Create enums for filling a struct fields
// MARK: Enum to describe sex
enum Sex {
    case Male, Female
}

// MARK: Enum to describe age category
enum AgeCategory {
    case Adolescent // 13 years through 17 years
    case Adults // 18 years through 65 years
    case OlderAdults // 65 years and older
}

// MARK: Enum to describe work experience
enum WorkExperience {
    case Junior, Middle, Senior
}

// MARK: Struct that describes from for employee
struct FormForEmployee {
    let firstName: String
    let lastName: String
    let age: UInt8
    let sex: Sex
    let ageCategory: AgeCategory
    let workExperience: WorkExperience
}


// MARK: - Create enum with rainbow colors
enum RainbowColors : CaseIterable {
    case Red, Orange, Yellow, Green, Blue, Indigo, Violet
}


// MARK: - Create a function that handles RainbowColors enum values
func rainbowColorsHandler(color: RainbowColors) {
    switch color {
        case .Red:
            print("Red apple")
        case .Orange:
            print("Orange orange")
        case .Yellow:
            print("Yellow lemon")
        case .Green:
            print("Green grass")
        case .Blue:
            print("Blue sea")
        case .Indigo:
            print("Indigo sky")
        case .Violet:
            print("Violet grape")
    }
}

print("\nExamples of things with specific color:")
RainbowColors.allCases.forEach { rainbowColorsHandler(color: $0) }


// MARK: - Create funciton that evaluate pupils
// MARK: Enum that describes scores
enum Score : String, CaseIterable {
    case A = "excellent"
    case B = "good"
    case C = "satisfactory"
    case D = "bad"
    case F = "unsatisfactory"
}

// MARK: Function that set the mark
func setScore(score: Score) {
    let scoreInNum: Int
    switch score {
        case .A: scoreInNum = 5
        case .B: scoreInNum = 4
        case .C: scoreInNum = 3
        case .D: scoreInNum = 3
        case .F: scoreInNum = 2
    }

    print("Your score is \(score) or \(scoreInNum). It means that you have done " +
          "\(score.rawValue) job")
}

print("\nsetScore function for an every possible score:")
Score.allCases.forEach { setScore(score: $0) }


// MARK: - Create a function that writes types of car bodies to console
// MARK: Enum that describes a type of car body
enum CarBodyType : CaseIterable {
    case Sedan
    case Coupe
    case SportsCar
    case Hatchback
    case Minivan
    case PickupTruck
    case Crossover
}

// MARK: Function that prints car body types
func viewInGarage(cars: [CarBodyType]) {
    var numOfCars = Dictionary(
        uniqueKeysWithValues: zip(
            CarBodyType.allCases, Array(repeating: 0, count: CarBodyType.allCases.count)
        )
    )

    cars.forEach { numOfCars[$0]! += 1 }
    numOfCars.forEach { print("\($0.key) -- \($0.value)") }
}

print("\nIn garage we have:")
viewInGarage(cars: [.Sedan, .Sedan, .Sedan, .Hatchback, .Coupe, .Crossover, .Hatchback])
